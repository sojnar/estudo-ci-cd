from flask import Flask, jsonify, request

app = Flask(__name__)

comments = [
    {
        "email":"alice@example.com",
        "comment":"first post!",
        "content_id":1
    },
    {
        "email":"alice@example.com",
        "comment":"ok, now I am gonna say something more useful",
        "content_id":2
    },
    {
        "email":"bob@example.com",
        "comment":"I agree",
        "content_id":1
    }
]

@app.route('/api/comment/list/<int:content_id>', methods=['GET'])
def comment_id(content_id):
    comment_id_number = [comment for comment in comments if comment['content_id'] == content_id ]
    return jsonify(comment_id_number), 200

@app.route('/api/comment/new', methods=['POST'])
def commnet_post():
    new_comment = request.get_json()
    comments.append(new_comment)
    return jsonify(new_comment), 201
    
if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")